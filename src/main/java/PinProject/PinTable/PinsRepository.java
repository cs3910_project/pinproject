package PinProject.PinTable;

import PinProject.PinTable.Pins;
import org.springframework.data.repository.CrudRepository;



// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface PinsRepository extends CrudRepository<Pins, Long> {

}
