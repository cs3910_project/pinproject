package PinProject.PinTable;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="pins")
public class Pins {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "pid", updatable = false, nullable = false)
    private Long pid;

    @Column(name="uid", nullable = false)
    private Long uid;

    @Column(name="pin", length = 6,nullable = false)
    private String pin;

    @Column(name="create_timestamp", nullable = false)
    @CreationTimestamp
    private Timestamp create_timestamp;

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public Timestamp getCreate_timestamp() {
        return create_timestamp;
    }

    public void setCreate_timestamp(Timestamp create_timestamp) {
        this.create_timestamp = create_timestamp;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }
}
