package PinProject.UserTable;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="users")
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(name="account",nullable = false)
    private String account;

    @Column(name="password",nullable = false)
    private String password;

    @Column(name="pin", length = 6,nullable = true)
    private String pin;

    @Column(name="pid", nullable = true)
    private Long pid;

    @Column(name="create_timestamp", nullable = true)
    private Timestamp create_timestamp;

    @Column(name="create_ip",nullable = true)
    private String create_ip;

    @Column(name="create_user",nullable = true)
    private String create_user;

    @Column(name="expire_timestamp",nullable = true)
    private Timestamp expire_timestamp;

    @Column(name="claim_timestamp",nullable = true)
    private Timestamp claim_timestamp;

    @Column(name="claim_user",nullable = true)
    private String claim_user;

    @Column(name="claim_ip",nullable = true)
    private String claim_ip;

    public Long getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }
    public void setAccount(String account) {
        this.account = account;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public Timestamp getCreate_timestamp() {
        return create_timestamp;
    }

    public void setCreate_timestamp(Timestamp create_timestamp) {
        this.create_timestamp = create_timestamp;
    }

    public String getCreate_ip() {
        return create_ip;
    }

    public void setCreate_ip(String create_ip) {
        this.create_ip = create_ip;
    }

    public String getCreate_user() {
        return create_user;
    }

    public void setCreate_user(String create_user) {
        this.create_user = create_user;
    }

    public Timestamp getExpire_timestamp() {
        return expire_timestamp;
    }

    public void setExpire_timestamp(Timestamp expire_timestamp) {
        this.expire_timestamp = expire_timestamp;
    }

    public Timestamp getClaim_timestamp() {
        return claim_timestamp;
    }

    public void setClaim_timestamp(Timestamp claim_timestamp) {
        this.claim_timestamp = claim_timestamp;
    }

    public String getClaim_user() {
        return claim_user;
    }

    public void setClaim_user(String claim_user) {
        this.claim_user = claim_user;
    }

    public String getClaim_ip() {
        return claim_ip;
    }

    public void setClaim_ip(String claim_ip) {
        this.claim_ip = claim_ip;
    }

}
